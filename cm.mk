# Boot animation
TARGET_SCREEN_HEIGHT := 1280
TARGET_SCREEN_WIDTH := 720

# Inherit some common CM stuff
$(call inherit-product, vendor/cm/config/common_full_phone.mk)

# Inherit device configuration
$(call inherit-product, device/zte/t617/full_t617.mk)

PRODUCT_NAME := cm_t617
BOARD_VENDOR := zte
PRODUCT_DEVICE := t617

PRODUCT_GMS_CLIENTID_BASE := android-zte

PRODUCT_MANUFACTURER := ZTE
PRODUCT_MODEL := ZTE T617

PRODUCT_BRAND := ZTE
TARGET_VENDOR := zte
TARGET_VENDOR_PRODUCT_NAME := T617
TARGET_VENDOR_DEVICE_NAME := t617
